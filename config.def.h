/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 8;        /* gap pixel between windows */
static const unsigned int snap      = 24;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "RobotoMono-Regular:size=12" };
static const char dmenufont[]       = "RobotoMono-Regular:size=12";
static const char col_gray1[]       = "#200720";
static const char col_gray2[]       = "#401337";
static const char col_gray3[]       = "#dddddd";
static const char col_cyan[]        = "#151515";
static const char col_brd_active[]  = "#775677";
static const unsigned int borderalpha = OPAQUE;
static const unsigned int selalpha = 0xe5;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2       },
	[SchemeSel]  = { col_gray3, col_cyan,  col_brd_active  },
};
static const char *colors_nofullscreen[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, "#000000", col_gray2       },
	[SchemeSel]  = { col_gray3, col_cyan,  col_brd_active  },
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, TRANSPARENT, borderalpha },
	[SchemeSel]  = { OPAQUE, selalpha, borderalpha },
};
static const unsigned int alphas_fullscr[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, OPAQUE, borderalpha },
	[SchemeSel]  = { OPAQUE, OPAQUE, borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod1Mask
#define WINMOD Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       0, KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           0, KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             0, KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, 0, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-o", "-m", dmenumon, NULL };
static const char *dmenucmd_transparent[] = { "dmenu_run", "-m", dmenumon, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *explorercmd[] = {"pcmanfm", NULL };
static const char *browsercmd[] = {"firefox", NULL };
static const char *changebg[] = {"nitrogen", "--random", "--set-auto", NULL };
static const char *discordcmd[] = {"discord", NULL };

static Key keys[] = {
	/* modifier            keysym,keycode function        argument */
	{ MODKEY,              XK_p,     0,  dmenu_run,      {0} },
	{ MODKEY,              XK_e,     0,  spawn,          {.v = explorercmd }},
	{ MODKEY,              XK_d,     0,  spawn,          {.v = discordcmd }},
	{ MODKEY,              XK_c,     0,  spawn,          {.v = browsercmd }},
	{ MODKEY|ShiftMask,    XK_Return,0,  spawn,          {.v = termcmd } },
	{ MODKEY,              XK_b,     0,  togglebar,      {0} },
	{ MODKEY,              XK_j,     0,  focusstack,     {.i = +1 } },
	{ MODKEY,              XK_k,     0,  focusstack,     {.i = -1 } },
	{ WINMOD,              XK_Up,    0,  incnmaster,     {.i = +1 } },
	{ WINMOD,              XK_Down,  0,  incnmaster,     {.i = -1 } },
	{ WINMOD,              XK_Left,  0,  setmfact,       {.f = -0.05} },
	{ WINMOD,              XK_Right, 0,  setmfact,       {.f = +0.05} },
	{ MODKEY,              XK_Return,0,  zoom,           {0} },
	{ MODKEY,              XK_Tab,   0,  focusstack,     {.i = +1} },
	{ MODKEY|ShiftMask,    XK_c,     0,  killclient,     {0} },
	{ MODKEY,              XK_t,     0,  setlayout,      {.v = &layouts[0]} },
	{ MODKEY,              XK_f,     0,  setlayout,      {.v = &layouts[1]} },
	{ MODKEY,              XK_m,     0,  setlayout,      {.v = &layouts[2]} },
	{ MODKEY,              XK_space, 0,  cyclelayout,    {.i = +1 } },
	{ MODKEY|ShiftMask,    XK_space, 0,  togglefloating, {0} },
	{ MODKEY,              XK_0,     0,  view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,    XK_0,     0,  tag,            {.ui = ~0 } },
	{ MODKEY,              XK_comma, 0,  focusmon,       {.i = -1 } },
	{ MODKEY,              XK_period,0,  focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,    XK_comma, 0,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,    XK_period,0,  tagmon,         {.i = +1 } },
	{ MODKEY,              XK_Left,  0,  view_adjacent,  {.i = -1 } },
	{ MODKEY,              XK_Right, 0,  view_adjacent,  {.i = +1 } },
	{ MODKEY|ShiftMask,    XK_Left,  0,  move_adjacent,  {.i = -1 } },
	{ MODKEY|ShiftMask,    XK_Right, 0,  move_adjacent,  {.i = +1 } },
	{ MODKEY|ShiftMask,    XK_q, 0,      quit,           {0} },
	{ MODKEY|ShiftMask,    XK_r, 0,      quit,           {1} },
	TAGKEYS(               10,                            0)
	TAGKEYS(               11,                            1)
	TAGKEYS(               12,                            2)
	TAGKEYS(               13,                            3)
	TAGKEYS(               14,                            4)
	TAGKEYS(               15,                            5)
	TAGKEYS(               16,                            6)
	TAGKEYS(               17,                            7)
	TAGKEYS(               18,                            8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        cyclelayout,    {.i = +1} },
	{ ClkLtSymbol,          0,              Button3,        cyclelayout,    {.i = -1} },
	{ ClkWinTitle,          0,              Button1,        togglewin,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        spawn,          {.v = termcmd } },
	{ ClkStatusText,        MODKEY,         Button1,        quit,           {0} },
	{ ClkStatusText,        ShiftMask,      Button1,        spawn,          {.v = changebg} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

